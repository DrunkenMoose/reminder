var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');
var ReminderWorker = require('./reminder-worker');

class ReminderClendar {
    static processMsg(userId, msg) {
        var text = msg;
        var words = text.split(' ');
        var reminderTimeObject = ReminderClendar.findTime(words[0], words[1]);
        text = text.substr(reminderTimeObject.countDeleteSymbols);
        ReminderClendar.createReminderTime(userId, reminderTimeObject, text);
    }

    static findTime(first, second) {
        var date;
        var time;
        var countDeleteSymbols = 0;

        if (first.includes('.') && second.includes(':')) {
            date = ReminderClendar.parseDate(first);
            time = ReminderClendar.parseTime(second);
            countDeleteSymbols = first.length + second.length + 2;
        } else {
            if (second.includes('.') && first.includes(':')) {
                date = ReminderClendar.parseDate(second);
                time = ReminderClendar.parseTime(first);
                countDeleteSymbols = first.length + second.length + 2;
            } else {
                if (first.includes('.')) {
                    date = ReminderClendar.parseDate(first);
                    countDeleteSymbols = first.length + 1;
                } else {
                    if (first.includes(':')) {
                        time = ReminderClendar.parseTime(first);
                        countDeleteSymbols = first.length + 1;
                    }
                }
            }
        }

        var result = {
            date: date,
            time: time,
            countDeleteSymbols: countDeleteSymbols
        }

        return result;
    }

    static parseDate(textDate) {
        var result = [-1, -1, -1];
        var arr = textDate.split('.');
        for (var i = 0; i < Math.min(3, arr.length); i++) {
            if (ReminderClendar.isNumber(arr[i])) {
                var value = parseInt(arr[i]);
                result[i] = arr[i];
            }
        }
        return result;
    }

    static parseTime(textTime) {
        var result = [-1, -1];
        var arr = textTime.split(':');
        for (var i = 0; i < Math.min(2, arr.length); i++) {
            if (ReminderClendar.isNumber(arr[i])) {
                var value = parseInt(arr[i]);
                result[i] = arr[i];
            }
        }
        return result;
    }

    static isNumber(text) {
        for (var i = 0; i < text.length; i++) {
            if (text[i] < '0' || text[i] > '9') return false;
        }
        return true;
    }

    static createReminderTime(userId, reminderTimeObject, msg) {
        var date = reminderTimeObject.date;
        var time = reminderTimeObject.time;
        if (date == 'undefined' && time == 'undefined') {
            VkSpeaker.sendMsg(userId, msgs.reminderTimerFormatError);
            return;
        }
        var sql = 'SELECT timeZone FROM users WHERE userId=\'' + userId + '\';';
        db.query(sql, function (err, rows, field) {
            var timeZone = parseInt(rows[0].timeZone);
            var result = new Date();
            var now = new Date();
            if (err) {
                VkSpeaker.sendMsg(userId, msgs.getTimeZoneError);
                return;
            }
            try {
                if (!time) {
                    time = [10, 0];
                }
                if (date) {
                    date[1] -= 1;
                    result.setMonth(date[1], date[0]);
                    if (date[2] !== -1) {
                        result.setFullYear(date[2]);
                    }
                    result.setHours(time[0], time[1]);
                    if (result.getTime() < now.getTime()) {
                        result.setFullYear(now.getFullYear() + 1);
                    }
                } else {
                    result.setHours(time[0], time[1], 0, 0);
                    if (result.getTime() < now.getTime()) {
                        result.setDate(result.getDate() + 1);
                    }
                }
                ReminderWorker.addNewReminder(userId, result.getTime(), msg);
            } catch(e) {
                VkSpeaker.sendMsg(userId, msgs.reminderCalendar);
            }
        })
    }

    static createReminder(userId, ReminderTime, msg) {
    }
}

module.exports = ReminderClendar;