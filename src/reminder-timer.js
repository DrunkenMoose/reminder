var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');
var ReminderWorker = require('./reminder-worker');

class ReminderTimer {
    static processMsg(userId, msg) {
        var reminderTimeObject = ReminderTimer.findTimer(msg);
        msg = msg.substr(reminderTimeObject.numNeedToDeleteSymbols);
        ReminderTimer.generateReminder(userId, msg, reminderTimeObject.h, reminderTimeObject.m);
    }

    static generateReminder(userId, msg, h, m) {
        if (!isNaN(h) && !isNaN(m) && (h > 0 || m > 0)) {
            var reminderTime = new Date();
            reminderTime.setHours(reminderTime.getHours() + h);
            reminderTime.setMinutes(reminderTime.getMinutes() + m);
            ReminderWorker.addNewReminder(userId, reminderTime.getTime(), msg);
        } else {
            VkSpeaker.sendMsg(userId, msgs.reminderTimerFormatError);
        }
    }

    static findTimer(msg) {
        var result = {
            h: 0,
            m: 0,
            numNeedToDeleteSymbols: 0
        }
        var words = msg.split(' ');
        var i = 1;
        while (i < words.length) {
            var word = words[i].toLowerCase();
            if (ReminderTimer.isHour(word) || ReminderTimer.isMinute(word)) {
                if (ReminderTimer.isNumber(words[i - 1])) {
                    var value = parseInt(words[i - 1]);
                    if (ReminderTimer.isHour(word)) {
                        result.h += value;
                    } else {
                        result.m += value;
                    }
                    result.numNeedToDeleteSymbols += words[i - 1].length + word.length + 2;
                }
            } else {
                break;
            }
            i += 2;
        }
        return result;
    }

    static isHour(text) {
        var variableHours = [
            'час',
            'часа',
            'часов',
            'ч'
        ];
        return variableHours.indexOf(text) !== -1;
    }

    static isMinute(text) {
        var variableMinutes = [
            'минут',
            'мин',
            'м',
            'минута'
        ];
        return variableMinutes.indexOf(text) !== -1;
    }

    static isNumber(text) {
        for (var i = 0; i < text.length; i++) {
            if (text[i] < '0' || text[i] > '9') return false;
        }
        return true;
    }
}

module.exports = ReminderTimer;