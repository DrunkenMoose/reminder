var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');
var db = require('./db');

class ReminderWorker {
    static addNewReminder(userId, reminderTime, reminderText) {
        var sqlAddReminder = 'INSERT INTO reminders (userId, reminderTime, reminderText) VALUES(\'' + userId + '\',\'' + reminderTime + '\',\'' + reminderText + '\');';
        db.query(sqlAddReminder, function (err, rows, field) {
            if (err) {
                VkSpeaker.sendMsg(userId, msgs.addNewReminderErrorInsert);
            } else {
                VkSpeaker.sendMsg(userId, msgs.addNewReminder);
            }
        });
    }

    static checkAvailableReminders() {
        var now = new Date().getTime();
        var sql = 'SELECT * FROM reminders WHERE reminderSended=0 AND reminderTime<\'' + now + '\'AND followed=1;';
        db.query(sql, function (err, rows, field) {
            if (err) {
                console.log(msgs.checkAvailableReminder);
            } else {
                ReminderWorker.workWithAvailableReminders(rows);
            }
        })
    }

    static workWithAvailableReminders(reminders) {
        for (var i = 0; i < reminders.length; i++) {
            VkSpeaker.sendMsg(reminders[i].userId, reminders[i].reminderText);
            ReminderWorker.updateSendedStatus(reminders[i].reminderId);
        }
    }

    static updateSendedStatus(reminderId) {
        var sql = 'UPDATE reminders SET reminderSended=\'1\' WHERE reminderId=\'' + reminderId + '\';';
        db.query(sql, function (err, rows, field) {
            if (err) {
                console.log(msg.updateSendedStatusError + reminderId);
            }
        })
    }
}

module.exports = ReminderWorker;