var db = require('./db');
var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');


class TimeZoner {
    static setTimeZoneForUser(userId, timeZone) {
        var sql = 'UPDATE users SET timeZone=' + timeZone + ' WHERE userId=\'' + userId + '\';';
        db.query(sql, function(err, rows, fields) {
            if (err) {
                VkSpeaker.sendMsg(userId, msgs.setTimeZoneError);
            }else{
                VkSpeaker.sendMsg(userId, msgs.setTimeZone + timeZone);//TODO add beutifuler for timeZone
            }
        })
    }

    static processTimeZone(userId, timeZone) {
        timeZone = timeZone.replace(/\s*/g, '');
        var timeZoneNum = parseInt(timeZone, 10);
        if (Math.abs(timeZoneNum) < 24) {
            TimeZoner.setTimeZoneForUser(userId, timeZoneNum);
        } else {
            VkSpeaker.sendMsg(userId, msgs.processTimeZone);
        }
    }

    static getTimeZoneForUser(userId) {
        var sql = 'SELECT timeZone FROM users WHERE userId=' + userId + ';';
        db.query(sql, function(err, rows, fields) {
            if (err) {
                VkSpeaker.sendMsg(userId, msg.getTimeZoneError);
            } else{
                var timeZone = rows[0]['timeZone'];
                VkSpeaker.sendMsg(userId, msgs.setTimeZone + timeZone);
            }
        })
    }
}


module.exports = TimeZoner;