var TimeZoner = require('./time-zoner');
var Follower = require('./follower');
var ReminderParser = require('./reminder-parser');
var ReminderWorker = require('./reminder-worker');


var testMsg = 'напомни 24.08 15:00 ничего там';


function processMsg(userId, msg) {
    if (msg.toLowerCase().startsWith('пояс')) {
        processTimeZoneMsg(userId, msg);
        return;
    }
    if (msg.toLowerCase().startsWith('напомни')) {
        ReminderParser.processMsg(userId, msg);
        return;
    }
}

function processTimeZoneMsg(userId, msg) {
    var timeZone = msg.substr(4);
    if (timeZone.length === 0) {
        TimeZoner.getTimeZoneForUser(userId);
    } else {
        TimeZoner.processTimeZone(userId, timeZone);
    }
}

setInterval(function() {
    ReminderWorker.checkAvailableReminders();
  }, 1000);

processMsg(101, testMsg);
// Follower.unfollow(100);