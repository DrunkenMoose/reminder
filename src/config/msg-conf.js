var msgs = {
    processTimeZone: 'Неверный формат часового пояса.',
    setTimeZone: 'Ваш текущий часовой пояс: ',
    setTimeZoneError: 'Ошибка установки часового пояса.',
    getTimeZoneError: 'Ошибка получения часового пояса',
    followSelectError: 'Ошибка при подписке (Select). Id=',
    followInsertError: 'Ошибка при подписке (Insert). Id=',
    follow: 'Добро пожаловать. Список моих функций:...',
    addNewReminderErrorSelect: 'Ошибка добавления напоминалки (Select).',
    addNewReminderErrorInsert: 'Ошибка добавления напоминалки (Insert).',
    addNewReminder: 'Напоминание добавлено.',
    checkAvailableReminder: 'Ошибка при получении доступных напоминалок.',
    updateSendedStatusError: 'Ошибка при обновлении статуса sended. ReminderID=',
    updateFollowedStatusError: 'Ошибка при обновлении статуса followed. ReminderId=',
    unfollowSelectError: 'Ошибка при получении reminderId unfollow пользователя. UserId=',
    reminderTimerFormatError: 'Не правильный формат ввода.',
    reminderCalendar: 'Ошибка обработки времени/даты'
}

module.exports = msgs;