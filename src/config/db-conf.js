var config = {
    user: 'root',
    host: '127.0.0.1',
    password: 'root',
    database: 'botDB',
    charset: 'utf8_general_ci'
};

module.exports = config;