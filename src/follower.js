var db = require('./db');
var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');

class Follower {
    static follow(userId) {
        var sqlSelect = 'SELECT * FROM users WHERE userId=\'' + userId + '\';';
        db.query(sqlSelect, function (err, rows, field) {
            if (err) {
                VkSpeaker.sendMsg(userId, msgs.followSelectError + userId)
            } else {
                if (rows.length === 0) {
                    var sqlInsert = 'INSERT INTO users (userId) VALUES (\'' + userId + '\');';
                    db.query(sqlInsert, function (err, rows, field) {
                        if (err) {
                            VkSpeaker.sendMsg(userId, msgs.followInsertError + userId);
                        } else {
                            VkSpeaker.sendMsg(userId, msgs.follow);
                        }
                    })
                } else {
                    Follower.updateStatusForRefollowUser(userId);
                }
            }
        })
    }

    static updateStatusForRefollowUser(userId) {
        console.log('reFollow');
        // var sql = 'SELECT reminderId FROM reminders'
    }

    static unfollow(userId) {
        var sql = 'SELECT reminderId FROM reminders WHERE userId=\'' + userId + '\';';
        db.query(sql, function (err, rows, field) {
            if (err) {
                VkSpeaker.sendMsg(msgs.unfollowSelectError + userId);
            } else {
                for (var i = 0; i < rows.length; i++) {
                    var reminderId = rows[i].reminderId
                    var sqlUpdate = 'UPDATE reminders SET followed=\'0\' WHERE reminderId=\'' + reminderId + '\';';
                    db.query(sqlUpdate, function (err, rows, field) {
                        if (err) {
                            VkSpeaker.sendMsg(msgs.updateFollowedStatusError + reminderId);
                        }
                    });
                }
            }
        });
    }
}


module.exports = Follower;