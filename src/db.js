var mysql = require('mysql');
var config = require('./config/db-conf');

db = mysql.createConnection({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
    charset: config.charset
  });
  
  module.exports = db;