var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');
var ReminderWorker = require('./reminder-worker');

class ReminderWeek {
    static processMsg(userId, weekDay, msg) {
        var text = msg;
        var reminderTime = ReminderWeek.getReminderTime(text.split(' ')[0]);
        if (reminderTime.fromText) {
            text = text.substr(reminderTime.h.length + reminderTime.m.length + 1).trim();
        }
        var reminderUnixTime = ReminderWeek.getUnixTimeAndAddReminder(userId, weekDay, reminderTime, text);
    }

    static getReminderTime(text) {
        if (text.includes(':') || text.includes('.')) {
            var i = 0;
            var hours = '', minutes = '';
            while (i < text.length && ReminderWeek.isDigit(text[i]) && hours.length < 3) {
                hours += text[i];
                i++;
            }
            if (text[i] === ':' || text[i] === '.') {
                i += 1;
                while (i < text.length && ReminderWeek.isDigit(text[i]) && minutes.length < 3) {
                    minutes += text[i];
                    i++;
                }
            }
            if (hours.length != 0 && minutes.length != 0) {
                return {
                    fromText: true,
                    h: hours,
                    m: minutes
                };
            }
        }
        return {
            fromText: false,
            h: '10',
            m: '0'
        };
    }

    static isDigit(c) {
        return (c >= '0' && c <= '9');
    }

    static getUnixTimeAndAddReminder(userId, weekDay, time, text) {
        var sqlGetUserTimeZone = 'SELECT timeZone FROM users WHERE userId=' + userId + ';';
        db.query(sqlGetUserTimeZone, function (err, row, fields) {
            if (err) {
                VkSpeaker.sendMsg(userId, msgs.addNewReminderErrorSelect);
            } else {
                var result = new Date();
                var timezone = parseInt(row[0]['timeZone'], 10);
                // result.setHours(result.getHours() + timezone); //TODO uncomment on the server
                result.setSeconds(0);
                result.setMilliseconds(0);
                var reminderH = time.h, reminderM = time.m;
                if (reminderM <= result.getMinutes()) {
                    result.setHours(result.getHours() + 1);
                }
                result.setMinutes(reminderM);
                if (reminderH < result.getHours()) {
                    result.setDate(result.getDate() + 1);
                }
                result.setHours(reminderH);
                while (result.getDay() != weekDay) {
                    result.setDate(result.getDate() + 1);
                }
                ReminderWorker.addNewReminder(userId, result.getTime(), text);
            }
        });
    }
}

module.exports = ReminderWeek;