var VkSpeaker = require('./vk-speaker');
var msgs = require('./config/msg-conf');
var ReminderWeek = require('./reminder-week');
var ReminderTimer = require('./reminder-timer');
var ReminderCalendar= require('./reminder-calendar');

class ReminderParser {
    static processMsg(userId, msg) {
        var text = msg.substr(8); //delete 'напомни'
        var firstWord = text.split(' ')[0].toLowerCase();
        var weekDay = ReminderParser.getNumberWeekDay(firstWord);

        if (weekDay !== -1) {
            weekDay = parseInt(weekDay / 2);
            text = text.substr(firstWord.length);
            ReminderWeek.processMsg(userId, weekDay, text.trim());
        } else {
            if (firstWord.startsWith('через')) {
                text = text.substr(firstWord.length);
                ReminderTimer.processMsg(userId, text.trim());
            } else {
                if (firstWord.includes('.') || firstWord.includes(':')) {
                    ReminderCalendar.processMsg(userId, text.trim());
                } else {
                    VkSpeaker.sendMsg(userId, msgs.reminderTimerFormatError);
                }
            }
        }
    }

    static getNumberWeekDay(day) {
        var days = [
            'вс', 'воскресенье',
            'пн', 'понедельник',
            'вт', 'вторник',
            'ср', 'среда',
            'чт', 'четверг',
            'пт', 'пятница',
            'сб', 'суббота'
        ];
        return days.indexOf(day);
    }
}

module.exports = ReminderParser;